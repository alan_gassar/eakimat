function toggleIcon(e) {
    $(e.target)
        .prev('.panel-heading')
        .find(".more-less")
        .toggleClass('glyphicon-plus glyphicon-minus');
}
$('.panel-group').on('hidden.bs.collapse', toggleIcon);
$('.panel-group').on('shown.bs.collapse', toggleIcon);

$('.carousel').carousel({
    interval: 5000
});

$(document).ready(function () {
    $("#contacts_slider").click(function () {
        if ($(this).hasClass("opened")) {
            $(this).animate({left: "-165px"});
            $(this).removeClass("opened");
            $(this).addClass("closed");
        } else {
            $(this).animate({left: "0px"});
            $(this).removeClass("closed");
            $(this).addClass("opened");
        }
    });

    $("#feedback_btn").click(function () {
        if ($("#feedback").hasClass("opened")) {
            $("#feedback").animate({left: "-265px"});
            $("#feedback").removeClass("opened");
            $("#feedback").addClass("closed");
        } else {
            $("#feedback").animate({left: "0px"});
            $("#feedback").removeClass("closed");
            $("#feedback").addClass("opened");
        }
        $("#feedback_icon").toggleClass('toggled')
    });
});